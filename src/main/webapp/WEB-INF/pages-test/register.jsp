<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="../../css/register_login.css">
	<title>
		注册
	</title>
</head>
<body>
<script language=javascript>
    function check() {

        var phoneNumber = document.getElementById("customer_phonenumber");
        var password = document.getElementById("customer_password");
        var postcode = document.getElementById("customer_postcode");
        var password_identify = document.getElementById("customer_password_identify");

        if(phoneNumber.value=="") {
            alert("请输入完整基本注册信息！");
            phoneNumber.focus();
            return false;
        }
        if(password.value=="") {
            alert("请输入完整基本注册信息！");
            password.focus();
            return false;
        }
        if(postcode.value=="") {
            alert("请输入完整基本注册信息！");
            postcode.focus();
            return false;
        }
        if(password_identify.value=="") {
            alert("请输入完整基本注册信息！");
            password_identify.focus();
            return false;
        }
        if(password.value != password_identify.value) {
            alert("密码不一致，请重新输入！");
            password_identify.value="";
            password_identify.focus();
            return false;
        }
        return true;
    }

    function chose(checkbox){
        var registerButton = document.getElementById("register_button");
        if(checkbox.checked == true){
            registerButton.disabled = "";
        }
        if(checkbox.checked == false){
            registerButton.disabled = "disabled";
        }
    }

    function goto(img){
        if(img.id == "user"){
            window.location.href = "/userImg.do";
        }
        else if(img.id == "cart"){
            window.location.href = "/shoppingCart.do";
        }
        else if(img.id == "homepage"){
            window.location.href = "/indexLoader.do";
        }
    }

    function gotoLogin() {
        window.location.href = "/login.do";
    }

</script>


<div class="a-center-section fadeInUp">
	<img src="../../images/reading.png" />
	<br><br>
	<div class="a-box">
		<form id="register" onsubmit="return check()" action="register.do" method = "post">
			<h1 class="a-row">创建账户</h1>
			<div class="a-row">
				<label>手机号码</label>
				<input type="tel" maxlength="20" id="customer_phonenumber" name="customer_phonenumber" class="a-input-text">
			</div>
			<div class="a-row">
				<label>邮编</label>
				<input type="text" maxlength="10" id="customer_postcode" name="customer_postcode" class="a-input-text">
			</div>
			<div class="a-row">
				<label>邮箱（可选）</label>
				<input type="email" maxlength="20" id="customer_email" name="customer_email" class="a-input-text">
			</div>
			<div class="a-row">
				<label>登录密码</label>
				<input type="password" maxlength="20" id="customer_password" name="customer_password" class="a-input-text">
			</div>
			<div class="a-row">
				<label>确认密码</label>
				<input type="password" maxlength="20" id="customer_password_identify" name="customer_password_identify" class="a-input-text">
			</div>
			<div name="legalAgreementCheckBox" class="a-row a-checkbox">
				<label>
					<input type="checkbox" name="legalAgreementCheckBox" value="false" onclick="chose(this)">
					<span class="a-label a-checkbox-label light-text">
						我已阅读并同意本网站的<a href="http://www.whu.edu.cn/">使用条件</a>及<a href="http://www.whu.edu.cn/">隐私声明</a>
					</span>
				</label>
			</div>
			<div class="a-row large-space">
				<input type="submit" id="register_button" name="register_button" value="注册" class="a-register-button" disabled="disabled">
			</div>
		</form>

		<div class="a-row">
			<div class="box">
				<span class="line"></span>
				<span class="light-text large-space">其他注册方式</span>
				<span class="line"></span>
			</div>
		</div>
		<div class="a-row">
			<a href="https://weixin.qq.com/"><button id="register_button_wechat" name="register_button_wechat" class="a-register-button-wechat">微信账号注册</button></a>
		</div>
		<div class="a-row large-space">
			<a href="https://im.qq.com/"><button id="register_button_qq" name="register_button_qq" class="a-register-button-qq">QQ账号注册</button></a>
		</div>
		<div class="a-row large-space">
			<HR style="FILTER: alpha(opacity=100,finishopacity=0,style=3)" width="100%" color=#E0E0E0 SIZE=3>
		</div>
		<div class="a-row large-space">
			<label class="light-text">已拥有账户？<a href="javascript:" onclick="gotoLogin()">登录</a></label>
		</div>
	</div>
</div>
<img src="../../images/homepage1.png" style="position:fixed; right:1%; top:5%" id="homepage" onclick="goto(this)">
<img src="../../images/user1.png" style="position:fixed; right:1%; top:16%" id="user" onclick="goto(this)">
<img src="../../images/cart1.png" style="position:fixed; right:1%; top:27%" id="cart" onclick="goto(this)">
</body>

</html>