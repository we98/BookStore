<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../../css/register_login.css">
<title>
登录
</title>
</head>
<body>
<script language=javascript>
	function check() {
		var phoneNumber = document.getElementById("customer_phonenumber");
		var password = document.getElementById("customer_password");
		if(phoneNumber.value=="")
		{
			alert("请输入登录信息！");
			phoneNumber.focus();
			return false;
		}
		if(password.value=="")
		{
			alert("请输入密码！");
			password.focus();
			return false;
		}
		return true;
	}

    function goto(img){
        if(img.id == "user"){
            window.location.href = "/userImg.do";
        }
        else if(img.id == "cart"){
            window.location.href = "/shoppingCart.do";
        }
        else if(img.id == "homepage"){
            window.location.href = "/indexLoader.do";
        }
    }

    function goto(img) {
        if (img.id == "user") {
            window.location.href = "/userImg.do";
        }
        else if (img.id == "cart") {
            window.location.href = "/shoppingCart.do";
        }
        else if (img.id == "homepage") {
            window.location.href = "/indexLoader.do";
        }
    }

    function gotoRegister() {
        window.location.href = "/register.do";
    }


</script>

<div class="a-center-section  fadeInUp">
	<img src="../../images/reading.png"/>
	<br><br>
	<div class="a-box">
		<form id="login" onsubmit="return check()" action="login.do" method = "post">
			<h1 class="a-row">登录</h1>
			<div class="a-row">
				<label>手机号码或邮箱地址</label>
				<input type="tel" maxlength="20" id="customer_phonenumber" name="customer_phonenumber" class="a-input-text">
			</div>
			<div class="a-row">
				<label>登录密码</label>
				<input type="password" maxlength="20" id="customer_password" name="customer_password" class="a-input-text">
			</div>
			<div class="a-row large-space">
				<input type="submit" id="login_button" name="login_button" value="登录" class="a-login-button">
			</div>
			<div name="rememberPwd" class=" a-row">
				<label>
					<input type="checkbox" name="rememberPwd" value="false">
					<span class="a-label a-checkbox-label light-text">
						记住密码
					</span>
				</label>
			</div>
		</form>

		<div class="a-row">
			<div class="box">
				<span class="line"></span>
				<span class="light-text large-space">更多登录方式</span>
				<span class="line"></span>
			</div>
		</div>
		<div class="a-row">
			<a href="https://weixin.qq.com/"><button id="login_button_wechat" name="login_button_wechat" class="a-login-button-wechat">微信账号登录</button></a>
		</div>
		<div class="a-row large-space">
			<a href="https://im.qq.com/"><button id="login_button_qq" name="login_button_qq" class="a-login-button-qq">QQ账号登录</button></a>
		</div>

		<div class="a-row large-space">
			<HR style="FILTER: alpha(opacity=100,finishopacity=0,style=3)" width="100%" color=#E0E0E0 SIZE=3>
		</div>
		<div class="a-row large-space">
			<label class="light-text">还没有账户？<a href="javascript:" onClick="gotoRegister()">注册</a></label>
		</div>
	</div>
</div>
<img src="../../images/homepage1.png" style="position:fixed; right:1%; top:5%" id="homepage" onclick="goto(this)">
<img src="../../images/user1.png" style="position:fixed; right:1%; top:16%" id="user" onclick="goto(this)">
<img src="../../images/cart1.png" style="position:fixed; right:1%; top:27%" id="cart" onclick="goto(this)">
</body>
</html>