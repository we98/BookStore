<%@ page import="bean.BookInfo" %>
<%@ page import="DaoImp.BookDaoImp" %>
<%@ page import="DaoImp.CommentDaoImp" %>
<%@ page import="bean.Comment" %>
<%@ page import="java.util.List" %>
<%@ page import="bean.Customer" %>
<%@ page import="DaoImp.CustomerDaoImp" %>
<%@ page import="Util.MoneyFormat" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <style>
        body{background: url(/images/bg.png);}
        html{
            height: 100%;
        }
        body{
            background: url(/images/bg.png);
            display: flex;
            flex-direction: column;
            height: 100%;
        }
        header{
            flex: 0 0 auto;
        }
        .main-content{
            flex: 1 0 auto;
        }
        footer{
            flex: 0 0 auto;
        }
    </style>
    <script>
        function goto(img){
            if(img.id == "user"){
                window.location.href = "/user";
            }
            else if(img.id == "cart"){
                window.location.href = "/shoppingCart";
            }
            else if(img.id == "homepage"){
                window.location.href = "/welcome";
            }
        }
        function addToCart(node) {
            var quantity = document.getElementById("quantity");
            var bookid = node.id;
            var returnURL = "/book";
            window.location.href = "/addToCart.do?bookid=" + bookid + "&quantity=" + parseInt(quantity.value) + "&returnURL=" + returnURL + "&from=book";
        }
        function add() {
            var quantity = document.getElementById("quantity");
            quantity.value = parseInt(quantity.value) + 1;
        }
        function subtract() {
            var quantity = document.getElementById("quantity");
            if(parseInt(quantity.value) > 1){
                quantity.value = parseInt(quantity.value) - 1;
            }
        }
        function gotoDutyPage() {
            window.location.href = "/duty";
        }
        $(function () {
            $("[data-toggle='popover']").popover();
        });
    </script>
</head>
<body data-spy="scroll" data-target="#myScrollspy">
<header>
    <div class="container">
        <div class="jumbotron">
            <h1>IReading</h1>
        </div>
    </div>

</header>
<div class="container main-content">
    <%
        BookInfo book = BookDaoImp.getBook(Integer.parseInt(request.getParameter("bookid")));
    %>
    <div class="row">
        <div class="col-sm-6 col-md-3" style="margin-left:100px">
            <img src="/images/<%=book.getSrc()%>" class="img-thumbnail">
        </div>
        <div class="col-sm-6 col-md-7">
            <h2 id="Title"><b><%=book.getName()%></b></h2>
            <hr style="height:1px;border:none;border-top:1px solid #555555;" />
            <h5 id="author">By: <%=book.getPublisher()%></h5>
            <hr style="height:1px;border:none;border-top:1px solid #C0C0C0;" />
            <h2 id="price" style="color:#87CEFA;">￥<%=MoneyFormat.printMoney(book.getPrice())%></h2>
            <hr style="height:1px;border:none;border-top:1px solid #C0C0C0;" />
            <form class="form-inline" role="form">
                <div class="form-group">
                    <label style="font-size: 17px;color:#A9A9A9;width: 60px"> 数量:  </label>
                </div>

                <div class="form-group" onclick="subtract()">
                    <input class="btn btn-default" name="subtract" id="subtract" type="button" value="-" />
                </div>

                <div class="form-group">
                    <label class="sr-only" for="quantity"> 名称 </label>
                    <input type="text" class="form-control" id="quantity" value=1 style="width: 40px">
                </div>

                <div class="form-group" onclick="add()">
                    <input class="btn btn-default" name="add" id="add" type="button" value="+"/>
                </div>
            </form>

            <div class="form-group" style="width: 250px">
                <button onclick="addToCart(this)" class="btn btn-success btn-lg" style="width: 200px" id="<%=book.getBookId()%>" title="添加成功"
                        data-container="body" data-toggle="popover" data-placement="top"
                        data-content="已在购物车里等您"> 加入购物车 </button>
            </div>

        </div>
    </div>
    <div class="row" style="margin-top:40px">
        <div class="col-sm-6 col-md-10" style="margin-left:100px">
            <blockquote style="font-size: 17px;color:#696969">
                <p>
                    <%=book.getDescription()%>
                </p>
            </blockquote>
        </div>
    </div>

    <div class="col-sm-6 col-md-10" style="margin-left:80px">
        <div>
            <h3>评论</h3>
            <hr style="height:1px;border:none;border-top:1px solid #555555;" />
        </div>
        <%
            List<Comment> list = CommentDaoImp.getCommentsByBook(book.getBookId());
            if (list.size() == 0) {


        %>
        <form class="form-inline" role="form">

            <div class="form-group">
                <h4>暂时还没有评论哦！</h4>
            </div>
            <hr style="height:1px;border:none;border-top:1px solid #C0C0C0;" />
        </form>
        <%
            } else {
                for (Comment comment: list) {

        %>
        <form class="form-inline" role="form">
            <div class="form-group" style="width: 80px">
                <img style="height: 60px" src="../../images/avatar.jpg" />
            </div>
            <div class="form-group">
                <h4><%=CustomerDaoImp.getName(comment.getCustomerId())%></h4>
                <%=comment.getCommentInfo()%>
            </div>
            <hr style="height:1px;border:none;border-top:1px solid #C0C0C0;" />
        </form>
        <%
                }
            }
        %>
    </div>


</div>

<a href="javascript:" onclick="goto(this)" id="homepage"><img src="/images/homepage1.png" style="position:fixed; right:1%; top:5%"></a>
<a href="javascript:" onclick="goto(this)" id="user"><img src="/images/user1.png" style="position:fixed; right:1%; top:16%"></a>
<a href="javascript:" onclick="goto(this)" id="cart"><img src="/images/cart1.png" style="position:fixed; right:1%; top:27%"></a>

<footer class="container" style="background-color: rgb(238, 238, 238);height: 100px; text-align:center;">
    <div style="margin-top: 10px"><a href="javascript:void(0)" onClick="gotoDutyPage()">使用条件</a>&nbsp&nbsp&nbsp<a href="javascript:void(0)" onClick="gotoDutyPage()">隐私声明</a></div>
    <h4 style="margin-top: 20px; color: #878B91;">
        Copyright &copy;2018 IReading
    </h4>
</footer>
</body>
</html>

