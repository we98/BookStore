<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="/css/register_login.css">
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<script src="/js/jquery.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<style>
		html{
			height: 100%;
		}
		body{
			display: flex;
			flex-direction: column;
			height: 100%;
		}
		header{
			flex: 0 0 auto;
		}
		.main-content{
			flex: 1 0 auto;
		}
		footer{
			flex: 0 0 auto;
		}
	</style>
	<title></title>
	<script language=javascript>
        function goto(img){
            if(img.id == "user"){
                window.location.href = "/register.do";
            }
            else if(img.id == "cart"){
                window.location.href = "/shoppingCart.do";
            }
            else if(img.id == "homepage"){
                window.location.href = "/indexLoader.do";
            }
        }
	</script>
</head>
<body>

<header>
	<div class="container">
		<div class="jumbotron">
			<h1>IReading</h1>
		</div>
	</div>
</header>

<div class="container main-content">
	<div class="row">
		<div class="col-sm-4 col-md-10 col-md-offset-1 panel panel-default">
			<div style="margin-bottom: 50px">
					<h3>计算机学院软件工程系2015级1-3班第7小组</h3>
				<blockquote style="font-size: 17px">
					<p>
						魏春光 2016302580238    李家君 2016302580243
					</p>
					<p>
						陈旭峰 2016302580241    艾泓飞 2016302580195
					</p>
					<p>
						王梓轩 2016302580327    彭东宇 2016302580285
					</p>
				</blockquote>
			</div>

			<div style="margin-bottom: 50px">
				<h3>IReading使用条件</h3>
				<blockquote style="font-size: 17px">
					<p>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						本站点由计算机学院软件工程系2015级1-3班第7小组制作，未经许可，
						任何人不得擅自（包括但不限于：以非法的方式复制、传播、展示、镜像、上载、下载）使用，
						或通过非常规方式（如：恶意干预本站数据）影响本站的正常服务，
						任何人不得擅自以软件程序自动获得本站数据。
						否则，本站 <s>将依法追究法律责任</s> <font color="red">也不能拿你咋样</font>。
					</p>
				</blockquote>
			</div>

			<div style="margin-bottom: 50px">
					<h3>IReading隐私声明</h3>
					<blockquote style="font-size: 17px">
						<h3><b>引言</b></h3>
						<p style="margin-bottom: 30px">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
							计算机学院软件工程系2015级1-3班第7小组（以下称 “我们”）非常重视用户个人信息及隐私权的保护,
							因此我们制订了涵盖如何收集、存储、使用、共享和保护用户信息的隐私政策。
							我们希望通过本隐私权保护声明向您清晰地介绍我们对您个人信息的处理方式，
							本隐私政策与您使用的产品和 / 或服务息息相关，请您在使用产品和 / 或服务前，
							完整阅读并透彻理解本隐私政策，并相应作出您认为适当的选择。
							<b><u>
								如果您不同意本隐私政策的任何内容，
								您应立即停止使用我们的产品和 / 或服务。当您使用我们任一产品和 / 或服务时，则表示您同意且完全理解本隐私政策的全部内容。
							</u></b>
						</p>

						<h3><b>信息的收集和使用</b></h3>
						<p style="margin-bottom: 30px">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							当您在注册本网站会员时，
							我们要求您必须以填表的形式提供有关信息，
							如您的地址，电话号码，电子邮件地址等，
							只要您在本网站注册成功并登陆服务器，我们将可以识别您的资料。我们将根据这些统计数据来给我们的会员分类，例如书籍推荐等，以便有针对性地向我们的会员提供新的服务和机会。我们将通过您的邮件地址来通知您——这些新服务和机会。
						</p>

						<h3><b>Cookies 的使用</b></h3>
						<p style="margin-bottom: 30px">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							本网站会使用 Cookies, 
							以便于能够为您提供更加周到的个性化服务。
							当您登陆我们的网站时，本网站会使用 Cookies 存储相关信息，
							追访您的个人喜好、使用习惯，使我们的网站在您再次访问时可以辨认您的身份，
							从而向您提供感兴趣的信息资料或储存密码，以便您造访本网站时不必每次重复输入密码。
							你也可以关闭此项功能，本网站将停止 Cookies 为您提供的服务。
						</p>

						<h3><b>隐私权的修订与意见反馈</b></h3>
						<p style="margin-bottom: 30px">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							如果你对本网站的隐私保护措施以及您在使用中的问题有任何意见和建议，请和我们联系：<font color="red">1320018234@QQ.com</font>。
							随着本网站服务范围的扩大，我们会随时更新我们的隐私声明。
							我们欢迎您随时来查看本声明。如果在用户信息政策方面有大幅度修改时，我们会在网页中明显位置贴出相关规定以便及时通知您。 
						</p>
					</blockquote>
				</div>
		</div>
	</div>
	<img src="../../images/homepage1.png" style="position:fixed; right:1%; top:5%" id="homepage" onclick="goto(this)">
	<img src="../../images/user1.png" style="position:fixed; right:1%; top:16%" id="user" onclick="goto(this)">
	<img src="../../images/cart1.png" style="position:fixed; right:1%; top:27%" id="cart" onclick="goto(this)">
</div>


<footer class="container" style="background-color: rgb(238, 238, 238);height: 110px; text-align:center;">
	<div style="margin-top: 20px"><a href="#">使用条件</a>&nbsp&nbsp&nbsp<a href="#">隐私声明</a></div>
	<h4 style="margin-top: 20px; color: #878B91;">
		Copyright &copy;2018 IReading
	</h4>
</footer>
</body>

</html>