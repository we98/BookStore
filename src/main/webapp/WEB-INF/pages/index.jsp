<%@ page import="bean.BookInfo" %>
<%@ page import="java.util.List" %>
<%@ page import="DaoImp.BookDaoImp" %>
<%@ page import="Util.MoneyFormat" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>welcome</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <style>
        body{background: url(/images/bg.png);}
        .test{
            height:165px;
            width:165px;
        }
        .book{
            height:80px;
            width:165px;
        }
        html{
            height: 100%;
        }
        body{
            display: flex;
            flex-direction: column;
            height: 100%;
        }
        header{
            flex: 0 0 auto;
        }
        .main-content{
            flex: 1 0 auto;
        }
        footer{
            flex: 0 0 auto;
        }
        ul.nav-tabs{
            width: 140px;
            margin-top: 20px;
            border-radius: 4px;
            border: 1px solid #ddd;
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.067);
        }
        ul.nav-tabs li{
            margin: 0;
            border-top: 1px solid #ddd;
        }
        ul.nav-tabs li:first-child{
            border-top: none;
        }
        ul.nav-tabs li a{
            margin: 0;
            padding: 8px 16px;
            border-radius: 0;
        }
        ul.nav-tabs li.active a, ul.nav-tabs li.active a:hover{
            color: #fff;
            background: #0088cc;
            border: 1px solid #0088cc;
        }
        ul.nav-tabs li:first-child a{
            border-radius: 4px 4px 0 0;
        }
        ul.nav-tabs li:last-child a{
            border-radius: 0 0 4px 4px;
        }
        ul.nav-tabs.affix{
            top: 30px; /* Set the top position of pinned element */
        }
    </style>

    <script>
        var originWidth;
        var originHeight;
        function bigger(img){
            originWidth = img.width;
            originHeight = img.height;
            img.width = img.width*1.1;
            img.height = img.height*1.1;
        }
        function smaller(img){
            img.width = originWidth;
            img.height = originHeight;
        }
        function goto(img){
            if(img.id == "user"){
                window.location.href = "/user";
            }
            else if(img.id == "cart"){
                window.location.href = "/shoppingCart";
            }
            else if(img.id == "homepage"){
                window.location.href = "/welcome";
            }
        }
        function gotoBookInformation(node) {
            /**
             * Modify by 李家君 on 2018.06.13
             */
            var bookid = node.parentNode.id;
            window.location.href = "/book?bookid="+bookid;
        }
        function addToCart(node) {
            var bookid = node.parentNode.id;
            var returnURL = "/welcome";
            window.location.href = "/addToCart.do?bookid=" + bookid + "&quantity=1" + "&returnURL=" + returnURL + "&from=index";
        }
        $(function () {
            $("[data-toggle='popover']").popover();
        });
        function gotoDutyPage() {
            window.location.href = "/duty";
        }
    </script>
</head>
<body data-spy="scroll" data-target="#myScrollspy">

    <header>
        <div class="container">
            <div class="jumbotron">
                <h1>IReading</h1>
            </div>
        </div>
    </header>

    <div class="container main-content">

        <div class="row">

            <div class="col-xs-3 col-md-3" id="myScrollspy">
                <ul class="nav nav-tabs nav-stacked" data-spy="affix" data-offset-top="125">
                    <li class="active"><a href="#section-1">IT精选</a></li>
                    <li><a href="#section-2">文海畅游</a></li>
                    <li><a href="#section-3">小说精粹</a></li>
                    <li><a href="#section-4">数学推理</a></li>
                </ul>
            </div>

            <div class="col-xs-9 col-md-9">

                <h2 id="section-1">IT精选</h2>
                <div class="row text-center">
                    <%
                        List<BookInfo> list = BookDaoImp.getBooksByType(1);
                        for (BookInfo book: list) {
                    %>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail">
                            <div class="test"><img src="/images/<%=book.getSrc()%>" alt="通用的占位符缩略图" height="150" width="150" onmouseover="bigger(this)" onmouseout="smaller(this)"></div>
                            <div class="caption">
                                <h3 class="book"><%=book.getName()%></h3>
                                <h4 style="color:rgb(130, 130, 130)">￥<%=MoneyFormat.printMoney(book.getPrice())%></h4>
                                <p id="<%=book.getBookId()%>">
                                    <a href="javascript:void(0);" onclick="gotoBookInformation(this)" class="btn btn-primary" role="button">
                                        查看
                                    </a>
                                    <a href="javascript:void(0);" onclick="addToCart(this)" class="btn btn-default" role="button" title="添加成功"
                                       data-container="body" data-toggle="popover" data-placement="top"
                                       data-content="已在购物车里等您">
                                        加入购物车
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <%
                        }
                        list = BookDaoImp.getBooksByType(2);
                    %>
                </div>

                <hr>
                <h2 id="section-2">文海畅游</h2>
                <div class="row text-center">
                    <%
                        for (BookInfo book: list) {
                    %>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail">
                            <div class="test"><img src="/images/<%=book.getSrc()%>" alt="通用的占位符缩略图" height="150" width="150" onmouseover="bigger(this)" onmouseout="smaller(this)"></div>
                            <div class="caption">
                                <h3 class="book"><%=book.getName()%></h3>
                                <h4 style="color:rgb(130, 130, 130)">￥<%=MoneyFormat.printMoney(book.getPrice())%></h4>
                                <p id="<%=book.getBookId()%>">
                                    <a href="javascript:void(0);" onclick="gotoBookInformation(this)" class="btn btn-primary" role="button">
                                        查看
                                    </a>
                                    <a href="javascript:void(0);" onclick="addToCart(this)" class="btn btn-default" role="button"  title="添加成功"
                                       data-container="body" data-toggle="popover" data-placement="top"
                                       data-content="已在购物车里等您">
                                        加入购物车
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <%
                        }
                        list = BookDaoImp.getBooksByType(3);
                    %>
                </div>

                <hr>
                <h2 id="section-3">小说精粹</h2>
                <div class="row text-center">
                    <%
                        for (BookInfo book: list) {
                    %>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail">
                            <div class="test"><img src="/images/<%=book.getSrc()%>" alt="通用的占位符缩略图" height="150" width="150" onmouseover="bigger(this)" onmouseout="smaller(this)"></div>
                            <div class="caption">
                                <h3 class="book"><%=book.getName()%></h3>
                                <h4 style="color:rgb(130, 130, 130)">￥<%=MoneyFormat.printMoney(book.getPrice())%></h4>
                                <p id="<%=book.getBookId()%>">
                                    <a href="javascript:void(0);" onclick="gotoBookInformation(this)" class="btn btn-primary" role="button">
                                        查看
                                    </a>
                                    <a href="javascript:void(0);" onclick="addToCart(this)" class="btn btn-default" role="button" title="添加成功"
                                       data-container="body" data-toggle="popover" data-placement="top"
                                       data-content="已在购物车里等您">
                                        加入购物车
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <%
                        }
                        list = BookDaoImp.getBooksByType(4);
                    %>
                </div>

                <hr>
                <h2 id="section-4">数学推理</h2>
                <div class="row text-center">
                    <%
                        for (BookInfo book: list) {
                    %>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail">
                            <div class="test"><img src="/images/<%=book.getSrc()%>" alt="通用的占位符缩略图" height="150" width="150" onmouseover="bigger(this)" onmouseout="smaller(this)"></div>
                            <div class="caption">
                                <h3 class="book"><%=book.getName()%></h3>
                                <h4 style="color:rgb(130, 130, 130)">￥<%=MoneyFormat.printMoney(book.getPrice())%></h4>
                                <p id="<%=book.getBookId()%>">
                                    <a href="javascript:void(0);" onclick="gotoBookInformation(this)" class="btn btn-primary" role="button">
                                        查看
                                    </a>
                                    <a href="javascript:void(0);" onclick="addToCart(this)" class="btn btn-default" role="button" title="添加成功"
                                       data-container="body" data-toggle="popover" data-placement="top"
                                       data-content="已在购物车里等您">
                                        加入购物车
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <%
                        }
                    %>
                </div>

            </div>
        </div>
    </div>

    <a href="javascript:" onclick="goto(this)" id="homepage"><img src="/images/homepage1.png" style="position:fixed; right:1%; top:5%"></a>
    <a href="javascript:" onclick="goto(this)" id="user"><img src="/images/user1.png" style="position:fixed; right:1%; top:16%"></a>
    <a href="javascript:" onclick="goto(this)" id="cart"><img src="/images/cart1.png" style="position:fixed; right:1%; top:27%"></a>

    <footer class="container" style="background-color: rgb(238, 238, 238);height: 110px; text-align:center;">
        <div style="margin-top: 10px"><a href="javascript:void(0)" onClick="gotoDutyPage()">使用条件</a>&nbsp&nbsp&nbsp<a href="javascript:void(0)" onClick="gotoDutyPage()">隐私声明</a></div>
        <h4 style="margin-top: 20px; color: #878B91;">
            Copyright &copy;2018 IReading
        </h4>
    </footer>


</body>
</html>