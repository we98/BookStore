<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="/css/register_login.css">
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<script src="/js/jquery.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<style>
		html{
			height: 100%;
		}
		body{
			display: flex;
			flex-direction: column;
			height: 100%;
		}
		header{
			flex: 0 0 auto;
		}
		.main-content{
			flex: 1 0 auto;
		}
		footer{
			flex: 0 0 auto;
		}
	</style>
	<script language=javascript>
        <%
        String errorInfo = (String)request.getAttribute("error");
        if(errorInfo == null || errorInfo == "none"){
            errorInfo = "none";
        }
        %>
        function check() {
            var phoneNumber = document.getElementById("phoneNumber");
            var password = document.getElementById("password");
            var confirm = document.getElementById("confirm");
            var legalAgreementCheckBox = document.getElementById("legalAgreementCheckBox");
            var province = document.getElementById("province");
            var city = document.getElementById("city");
            var district = document.getElementById("district");
            if(phoneNumber.value=="" || password.value=="" || confirm.value=="") {
                alert("请输入完整基本注册信息！");
                phoneNumber.focus();
                return false;
            }
            if(password.value != confirm.value) {
                alert("密码不一致，请重新输入！");
                confirm.value="";
                confirm.focus();
                return false;
            }
            if(legalAgreementCheckBox.checked != true){
                alert("请阅读本站的使用条件及隐私声明后勾选复选框！");
                return false;
			}
            if(province.selectedIndex==0 || city.selectedIndex==0 || district.selectedIndex==0) {
                alert("请选择地址！");
                return false;
            }

            return true;
        }
        function goto(img){
            if(img.id == "user"){
                window.location.href = "/user";
            }
            else if(img.id == "cart"){
                window.location.href = "/shoppingCart";
            }
            else if(img.id == "homepage"){
                window.location.href = "/welcome";
            }
        }
        function gotoLogin() {
            window.location.href = "/login";
        }

        function loadCity(myProvince) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    city(myProvince,this);
                }
            };
            xhttp.open("GET", "/resource/address.xml", true);
            xhttp.send();
        }

        function city(myProvince,xml) {
            var i;
            var xmlDoc = xml.responseXML;
            var provinces = xmlDoc.getElementsByTagName("province");
            var x;
            for(i = 0; i < provinces.length; ++i) {
                if(provinces[i].getAttribute("name") == myProvince) {
                    x = provinces[i].getElementsByTagName("city");
                    break;
                }
            }
            var temp = "<option value=\"请选择\">请选择</option>";
            for (i = 0; i <x.length; i++) {
                temp += "<option value='" + x[i].getAttribute("name") +"'>" + x[i].getAttribute("name") + "</option>";
            }
            document.getElementById("city").innerHTML = temp;
        }

        function loadDistrict(myCity) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    district(myCity,this);
                }
            };
            xhttp.open("GET", "/resource/address.xml", true);
            xhttp.send();
        }
        function district(myCity,xml) {
            var i;
            var xmlDoc = xml.responseXML;
            var cities = xmlDoc.getElementsByTagName("city");
            var x;
            for(i = 0; i < cities.length; ++i) {
                if(cities[i].getAttribute("name") == myCity) {
                    x = cities[i].getElementsByTagName("district");
                    break;
                }
            }
            var temp = "<option value=\"请选择\">请选择</option>";
            for (i = 0; i <x.length; i++) {
                temp += "<option value='" + x[i].getAttribute("name") +"'>" + x[i].getAttribute("name") + "</option>";
            }
            document.getElementById("district").innerHTML = temp;
        }
        function gotoDutyPage() {
            window.location.href = "/duty";
        }
	</script>
</head>
<body>

<header>
	<div class="container">
		<div class="jumbotron">
			<h1>IReading</h1>
		</div>
	</div>
</header>

<div class="container main-content">
	<div class="row">
		<div class="col-sm-4 col-md-4 col-md-offset-4 panel panel-default">
			<form id="register" onsubmit="return check()" action="register.do" method = "post" role="form">
				<h1>创建账户</h1>
				<div class="form-group">
					<label for="phoneNumber">手机号码</label>
					<input type="text" class="form-control" maxlength="20" id="phoneNumber" name="phoneNumber" placeholder="请输入手机号码">
				</div>
				<div class="form-group" id="errorInfo" style="display: <%=errorInfo%>">
					<h5 style="color: #CE0000">该账号已注册，请直接进行登录！</h5>
				</div>
				<div class="form-group">
					<label for="email">邮箱（可选）</label>
					<input type="email" class="form-control" maxlength="20" id="email" name="email" placeholder="请输入手机邮箱">
				</div>
				<div class="form-group">
					<label for="password">登录密码</label>
					<input type="password" class="form-control" maxlength="20" id="password" name="password" placeholder="请输入登录密码">
				</div>
				<div class="form-group">
					<label for="confirm">确认密码</label>
					<input type="password" class="form-control" maxlength="20" id="confirm" placeholder="请确认登录密码">
				</div>
				<div class="form-group">
					<label>选择地址</label>
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<select name="province" id="province" class="form-control" onchange="loadCity(this.value)">
								<option value="请选择">请选择</option><option value="北京">北京</option><option value="上海">上海</option>
								<option value="天津">天津</option><option value="重庆">重庆</option><option value="河北">河北</option>
								<option value="山西">山西</option><option value="河南">河南</option><option value="辽宁">辽宁</option>
								<option value="吉林">吉林</option><option value="黑龙江">黑龙江</option><option value="内蒙古">内蒙古</option>
								<option value="江苏">江苏</option><option value="山东">山东</option><option value="安徽">安徽</option>
								<option value="浙江">浙江</option><option value="福建">福建</option><option value="湖北">湖北</option>
								<option value="湖南">湖南</option><option value="广东">广东</option><option value="广西">广西</option>
								<option value="江西">江西</option><option value="四川">四川</option><option value="海南">海南</option>
								<option value="贵州">贵州</option><option value="云南">云南</option><option value="西藏">西藏</option>
								<option value="陕西">陕西</option><option value="甘肃">甘肃</option><option value="青海">青海</option>
								<option value="宁夏">宁夏</option><option value="新疆">新疆</option><option value="台湾">台湾</option>
								<option value="香港">香港</option><option value="澳门">澳门</option>
							</select>
						</div>
						<div class="col-sm-4 col-md-4">
							<select name="city" id="city" class="form-control" onchange="loadDistrict(this.value)">
								<option value="请选择">请选择</option>
							</select>
						</div>
						<div class="col-sm-4 col-md-4">
							<select name="district" class="form-control" id="district">
								<option value="请选择">请选择</option>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>
						<input type="checkbox" id="legalAgreementCheckBox">
						<span class="a-label a-checkbox-label light-text">
						我已阅读并同意本网站的<a href="http://www.whu.edu.cn/">使用条件</a>及<a href="http://www.whu.edu.cn/">隐私声明</a>
						</span>
					</label>
				</div>

				<div>
					<input type="submit" id="register_button" value="注册" class="btn btn-primary btn-lg btn-block" >
				</div>
			</form>
			<br>
			<label class="light-text">已拥有账户？<a href="javascript:" onclick="gotoLogin()">登录</a></label>
		</div>
	</div>

</div>
<a href="javascript:" onclick="goto(this)" id="homepage"><img src="/images/homepage1.png" style="position:fixed; right:1%; top:5%"></a>
<a href="javascript:" onclick="goto(this)" id="user"><img src="/images/user1.png" style="position:fixed; right:1%; top:16%"></a>
<a href="javascript:" onclick="goto(this)" id="cart"><img src="/images/cart1.png" style="position:fixed; right:1%; top:27%"></a>

<footer class="container" style="background-color: rgb(238, 238, 238);height: 110px; text-align:center;">
	<div style="margin-top: 10px"><a href="javascript:void(0)" onClick="gotoDutyPage()">使用条件</a>&nbsp&nbsp&nbsp<a href="javascript:void(0)" onClick="gotoDutyPage()">隐私声明</a></div>
	<h4 style="margin-top: 20px; color: #878B91;">
		Copyright &copy;2018 IReading
	</h4>
</footer>
</body>

</html>