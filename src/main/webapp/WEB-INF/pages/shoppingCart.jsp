<%@ page import="bean.BookInfo" %>
<%@ page import="java.util.List" %>
<%@ page import="bean.BookItem" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="Util.MoneyFormat" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
    <head>
    	<meta charset="UTF-8">
    	<title>Shopping Cart</title>
	    <link rel="stylesheet" href="/css/bootstrap.min.css">
	    <script src="/js/jquery.min.js"></script>
	    <script src="/js/bootstrap.min.js"></script>
	    <style>
    		body{background: url(/images/bg.png);}
    		ul.nav-tabs{
	    		width: 140px;
	    		margin-top: 20px;
	    		border-radius: 4px;
	    		border: 1px solid #ddd;
	    		box-shadow: 0 1px 4px rgba(0, 0, 0, 0.067);
	    	}
	    	ul.nav-tabs li{
	    		margin: 0;
	    		border-top: 1px solid #ddd;
	    	}
	    	ul.nav-tabs li:first-child{
	    		border-top: none;
		    }
		    ul.nav-tabs li a{
			    margin: 0;
                padding: 8px 16px;
			    border-radius: 0;
	    	}
	    	ul.nav-tabs li.active a, ul.nav-tabs li.active a:hover{
	    		color: #fff;
	    		background: #0088cc;
	    		border: 1px solid #0088cc;
	    	}
	    	ul.nav-tabs li:first-child a{
	    		border-radius: 4px 4px 0 0;
	    	}
	    	ul.nav-tabs li:last-child a{
	    		border-radius: 0 0 4px 4px;
	    	}
	    	ul.nav-tabs.affix{
	    		top: 30px; /* Set the top position of pinned element */
	    	}
			html{
				height: 100%;
			}
			body{
				display: flex;
				flex-direction: column;
				height: 100%;
			}
			header{
				flex: 0 0 auto;
			}
			.main-content{
				flex: 1 0 auto;
			}
			footer{
				flex: 0 0 auto;
			}
    	</style>
		<script>
			<%
			String isEnough = (String) request.getAttribute("isEnough");
			if(isEnough != null && isEnough == "false"){
			%>
			alert("您的余额不足，请联系管理员充值！")
			<%
			}
			%>
			function goto(img){
            	if(img.id == "user"){
                	window.location.href = "/user";
				}
            	else if(img.id == "cart"){
                	window.location.href = "/shoppingCart";
            	}
            	else if(img.id == "homepage"){
                	window.location.href = "/welcome";
            	}
        	}
        	function changeQuantity(node) {
				if(node.id=="toZero"){
                    window.location.href = "/modifyQuantity.do?difference=0&bookid=" + node.name;
				}
				else if(node.id=="substract"){
                    window.location.href = "/modifyQuantity.do?difference=-1&bookid=" + node.name;
				}
				else if(node.id=="add"){
                    window.location.href = "/modifyQuantity.do?difference=1&bookid=" + node.name;
				}
            }
            function clearItems() {
                window.location.href = "/clearShoppingCart.do";
            }
            function purchase() {
                window.location.href = "/shoppingCart.do";
            }
            function gotoDutyPage() {
                window.location.href = "/duty";
            }
		</script>
    </head>

	<body data-spy="scroll" data-target="#myScrollspy">
		<header>
			<div class="container">
				<div class="jumbotron">
					<h1>IReading</h1>
				</div>
			</div>
		</header>

		<div class="container main-content">
			<div class="row">
				<div class="col-sm-6 col-md-10" style="margin-left:100px">
					<div class="row">
						<table class="table">
							<thead>
								<tr>
									<th>
										商品
									</th>
									<th>
										价格
									</th>
									<th>
										数量
									</th>
									<th>
										商品小计
									</th>
								</tr>
							</thead>
							<tbody>
							<%
								List<BookItem> list = new ArrayList<>();
								float money = 0.0f;
								if (session.getAttribute("cartItems") != null)
									list.addAll((List<BookItem>)session.getAttribute("cartItems"));

								for(BookItem item: list) {
								    money += item.getMoney();
							%>
								<tr>
									<td>
										<form class="form-inline" role="form">
											<div class="form-group">
												<img src="<%="/images/"+item.getInfo().getSrc()%>" style="height:120px; width:120px">
											</div>
											<div class="form-group" style="margin-left:10px">
												<h4><b><%=item.getInfo().getName()%></b></h4>
												<h5 style="color:#808080"><%=item.getInfo().getPublisher()%></h5>
												<input class="btn btn-danger btn-xs" name="<%=item.getBookId()%>" type="button" value="X" id="toZero" onclick="changeQuantity(this)" />
											</div>
										</form>
									</td>
									<td>
										<h4 style="color:#808080;margin-top: 55px"><%=MoneyFormat.printMoney(item.getInfo().getPrice())%></h4>
									</td>
									<td>
										<form class="form-inline" role="form">
											<div class="form-group" style="margin-top:45px">
												<input class="btn btn-default btn-xs" name="<%=item.getBookId()%>" type="button" value="-" id="substract" onclick="changeQuantity(this)" />
											</div>
											<div class="form-group">
												<h4 style="color:#808080;margin-top: 55px"><%=item.getQuantity()%></h4>
											</div>
											<div class="form-group" style="margin-top:45px">
													<input class="btn btn-default btn-xs" name="<%=item.getBookId()%>" type="button" value="+" id="add" onclick="changeQuantity(this)"/>
											</div>
										</form>
									</td>
									<td>
										<h4 style="color:#808080;margin-top: 55px"><%=MoneyFormat.printMoney(item.getMoney())%></h4>
									</td>
								</tr>
								<%
									}
								%>

							</tbody>
						</table>
						<table class="table">
							<tbody>
								<tr>
									<td>
										<strong>小计</strong>
									</td>
									<td style="color:#808080;padding-left:790px">
									    <%=MoneyFormat.printMoney(money)%>
									</td>
								</tr>
								<tr>
									<td>
										<strong>运费</strong>
									</td>
									<td style="color:#808080;padding-left:790px">
										免运费
									</td>
								</tr>
								<tr>
									<td>
										<strong>总计</strong>
									</td>
									<td style="color:#808080;padding-left:790px">
										<%=MoneyFormat.printMoney(money)%>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="form-inline text-center" style="margin-bottom: 20px">

							<div class="form-group">
								<%--这里的onclick方法添加一个处理付账的servlet--%>
								<button onclick="purchase()" class="btn btn-primary btn-lg" style="width: 220px">立即支付</button>
							</div>
							<div class="form-group">
								<button class="btn btn-warning btn-lg" style="width: 220px" onclick="clearItems()">清空购物车</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<a href="javascript:" onclick="goto(this)" id="homepage"><img src="/images/homepage1.png" style="position:fixed; right:1%; top:5%"></a>
		<a href="javascript:" onclick="goto(this)" id="user"><img src="/images/user1.png" style="position:fixed; right:1%; top:16%"></a>
		<a href="javascript:" onclick="goto(this)" id="cart"><img src="/images/cart1.png" style="position:fixed; right:1%; top:27%"></a>

		<footer class="container" style="background-color: rgb(238, 238, 238);height: 110px; text-align:center;">
			<div style="margin-top: 10px"><a href="javascript:void(0)" onClick="gotoDutyPage()">使用条件</a>&nbsp&nbsp&nbsp<a href="javascript:void(0)" onClick="gotoDutyPage()">隐私声明</a></div>
			<h4 style="margin-top: 20px; color: #878B91;">
				Copyright &copy;2018 IReading
			</h4>
		</footer>
	</body>
</html>