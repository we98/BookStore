<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="/css/register_login.css">
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<script src="/js/jquery.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<style>
		html{
			height: 100%;
		}
		body{
			display: flex;
			flex-direction: column;
			height: 100%;
		}
		header{
			flex: 0 0 auto;
		}
		.main-content{
			flex: 1 0 auto;
		}
		footer{
			flex: 0 0 auto;
		}
	</style>
	<%--<script language=javascript>--%>
        <%--/**--%>
         <%--* 根据request修改输入框的placeholder--%>
         <%--* @author 李家君--%>
         <%--*/--%>
        <%--var phone = "${requestScope[\"phoneNumber\"]}";--%>
        <%--var pswd = "${requestScope[\"password\"]}";--%>

        <%--if(phone !== "null" && pswd !== "null"){--%>
            <%--var phoneNumber = document.getElementById("phoneNumber");--%>
            <%--var password = document.getElementById("password");--%>

            <%--phoneNumber.value = phone;--%>
            <%--password.value = pswd;--%>
        <%--}--%>
	<%--</script>--%>
	<script language=javascript>
        <%
        String errorInfo = (String)request.getAttribute("error");
        if(errorInfo == null || errorInfo == "none"){
            errorInfo = "none";
        }
		%>
        function check()
        {
            var phoneNumber = document.getElementById("phoneNumber");
            var password = document.getElementById("password");
            if(phoneNumber.value=="" || password.value=="")
            {
                alert("请输入完整基本登录信息！");
                return false;
            }
            return true;
        }
        function goto(img){
            if(img.id == "user"){
                window.location.href = "/user";
            }
            else if(img.id == "cart"){
                window.location.href = "/shoppingCart";
            }
            else if(img.id == "homepage"){
                window.location.href = "/welcome";
            }
        }
        function gotoRegister() {
            window.location.href = "/register";
        }
        function gotoDutyPage() {
            window.location.href = "/duty";
        }
	</script>
</head>
<body>

<header>
	<div class="container">
		<div class="jumbotron">
			<h1>IReading</h1>
		</div>
	</div>
</header>

<div class="container main-content">
	<div class="row">
		<div class="col-sm-4 col-md-4 col-md-offset-4 panel panel-default">
			<form id="register" onsubmit="return check()" action="login.do" method = "post" role="form">
				<h1>登录</h1>
				<div class="form-group">
					<label for="phoneNumber">手机号码</label>
					<input type="text" class="form-control" maxlength="20" id="phoneNumber" name="customer_phonenumber" placeholder="请输入手机号码">
				</div>
				<div class="form-group">
					<label for="password">登录密码</label>
					<input type="password" class="form-control" maxlength="20" id="password" name="customer_password" placeholder="请输入登录密码">
				</div>
				<div class="form-group" id="errorInfo" style="display: <%=errorInfo%>">
					<h5 style="color: #CE0000">您输入的账号或密码不正确，请重新输入！</h5>
				</div>
				<div>
					<input type="submit" id="register_button" value="登录" class="btn btn-primary btn-lg btn-block" >
				</div>
			</form>
			<br>
			<label class="light-text">还没有账户？<a href="javascript:" onClick="gotoRegister()">注册</a></label>
		</div>
	</div>

</div>

<a href="javascript:" onclick="goto(this)" id="homepage"><img src="/images/homepage1.png" style="position:fixed; right:1%; top:5%"></a>
<a href="javascript:" onclick="goto(this)" id="user"><img src="/images/user1.png" style="position:fixed; right:1%; top:16%"></a>
<a href="javascript:" onclick="goto(this)" id="cart"><img src="/images/cart1.png" style="position:fixed; right:1%; top:27%"></a>
<footer class="container" style="background-color: rgb(238, 238, 238);height: 110px; text-align:center;">
	<div style="margin-top: 10px"><a href="javascript:void(0)" onClick="gotoDutyPage()">使用条件</a>&nbsp&nbsp&nbsp<a href="javascript:void(0)" onClick="gotoDutyPage()">隐私声明</a></div>
	<h4 style="margin-top: 20px; color: #878B91;">
		Copyright &copy;2018 IReading
	</h4>
</footer>
</body>

</html>