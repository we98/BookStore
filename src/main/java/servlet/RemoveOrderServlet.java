package servlet;

import DaoImp.OrderDaoImp;
import DaoImp.ShoppingCartDaoImp;
import bean.BookItem;
import bean.Customer;
import bean.Order;
import com.sun.org.apache.xpath.internal.operations.Or;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class RemoveOrderServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        List<Order> orders = (List<Order>) session.getAttribute("orders");
        int orderId = Integer.parseInt(request.getParameter("orderId"));
        OrderDaoImp.removeOrderId(orderId);
        for (int i = 0; i < orders.size(); i++) {
            Order order = orders.get(i);
            if (order.getOrderId() == orderId) {
                orders.remove(i);
                break;
            }
        }
        session.setAttribute("orders", orders);
        request.getRequestDispatcher("/WEB-INF/pages/customer_information.jsp").forward(request,response);

    }
}
