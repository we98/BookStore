/**
 * @brief 用户退出回调
 * @author 李家君
 * @date 2018.06.18
 */


package servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CustomerExitServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        session.removeAttribute("orders");
        session.removeAttribute("currentCustomer");
        session.removeAttribute("cartItems");
        // 返回登陆页面
        req.getRequestDispatcher("/login").forward(req, resp);
    }
}
