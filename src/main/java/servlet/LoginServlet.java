/***
 *  Modify by 李家君 on 2018.06.13
 * 实现doPost方法
 */

package servlet;

import DaoImp.CustomerDaoImp;
import DaoImp.OrderDaoImp;
import DaoImp.ShoppingCartDaoImp;
import bean.Customer;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=utf-8");

        HttpSession session = request.getSession();
        String cPhone = request.getParameter("customer_phonenumber");
        String cPassword = request.getParameter("customer_password");

        Customer customer = CustomerDaoImp.find(cPhone, cPassword);         // 从数据库中读取生成的customerID
        if(customer == null){
            request.setAttribute("error", "inline");
            //response.sendRedirect("/login?error=yes");
            request.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(request,response);
        }
        else{
            session.setAttribute("currentCustomer", customer);
            session.setAttribute("cartItems", ShoppingCartDaoImp.getCustomerItem(customer));
            session.setAttribute("orders", OrderDaoImp.getOrders(customer.getId()));
            request.getRequestDispatcher("/WEB-INF/pages/index.jsp").forward(request,response);
        }
    }
}
