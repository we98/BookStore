package servlet;

import DaoImp.ShoppingCartDaoImp;
import bean.BookItem;
import bean.Customer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ClearShoppingCartServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        List<BookItem> cartItems = (List<BookItem>) session.getAttribute("cartItems");
        if(cartItems != null && cartItems.size() != 0){
            for (BookItem item : cartItems) {
                ShoppingCartDaoImp.removeItem(item, ((Customer)session.getAttribute("currentCustomer")).getId());
            }
            cartItems.clear();
            session.setAttribute("cartItems", cartItems);
        }
        request.getRequestDispatcher("/WEB-INF/pages/shoppingCart.jsp").forward(request,response);

    }

}
