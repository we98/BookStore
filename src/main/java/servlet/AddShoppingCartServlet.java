/**
 * @brief 书本项类（对应数据库的CartItem表、OrderItem表）
 * @author 李家君
 * @date 2018.06.13
 */

package servlet;

import DaoImp.BookDaoImp;
import bean.BookItem;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.print.Book;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AddShoppingCartServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        int bookid = Integer.parseInt(request.getParameter("bookid"));
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        String from = request.getParameter("from");

        List<BookItem> cartItems = (List<BookItem>) session.getAttribute("cartItems");
        if (cartItems == null)                                 // 购物车为空时新建购物车
            cartItems = new ArrayList<>();

        boolean isAdded = false;
        for (int i = 0; i < cartItems.size(); i++) {
            BookItem item = cartItems.get(i);
            if (item.getInfo().getBookId() == bookid) {         // 若原来已经选过该商品则在原基础上增加
                isAdded = true;
                item.addQuantity(quantity);
            }
        }

        if (!isAdded) {                                       // 若没有则新建商品项
            BookItem newItem = new BookItem();
            newItem.setInfo(BookDaoImp.getBook(bookid));
            newItem.setQuantity(quantity);
            newItem.setBookId(bookid);
            cartItems.add(newItem);
        }
        session.setAttribute("cartItems", cartItems);

        // 跳转回原来页面
        if(from.equals("index")){
            request.getRequestDispatcher(request.getParameter("returnURL")).forward(request,response);
        }
        else if(from.equals("book")){
            request.getRequestDispatcher(request.getParameter("returnURL") + "?bookid=" + bookid).forward(request,response);
        }
    }
}
