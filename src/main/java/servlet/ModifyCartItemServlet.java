package servlet;

import DaoImp.ShoppingCartDaoImp;
import bean.BookItem;
import bean.Customer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ModifyCartItemServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        int difference = Integer.parseInt(request.getParameter("difference"));
        int bookid = Integer.parseInt(request.getParameter("bookid"));

        List<BookItem> cartItems = (List<BookItem>) session.getAttribute("cartItems");

        for (int i = 0; i < cartItems.size(); i++) {
            BookItem item = cartItems.get(i);
            if (item.getInfo().getBookId() == bookid) {         // 若原来已经选过该商品则在原基础上增加
                if(difference != 0){
                    item.setQuantity(item.getQuantity() + difference);
                    if(item.getQuantity() == 0){
                        cartItems.remove(i);
                        ShoppingCartDaoImp.removeItem(item, ((Customer)session.getAttribute("currentCustomer")).getId());
                    }
                }
                else {
                    cartItems.remove(i);
                    ShoppingCartDaoImp.removeItem(item, ((Customer)session.getAttribute("currentCustomer")).getId());
                }
                break;
            }
        }
        session.setAttribute("cartItems", cartItems);
        request.getRequestDispatcher("/WEB-INF/pages/shoppingCart.jsp").forward(request,response);

    }
}