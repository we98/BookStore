/***
 *  Modify by 李家君 on 2018.06.13
 * 实现doPost方法
 */

package servlet;

import DaoImp.BookDaoImp;
import bean.BookInfo;
import bean.BookItem;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BookInformationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html; charset=utf-8");

        BookInfo info = BookDaoImp.getBook(Integer.parseInt(request.getParameter("bookid")));
        request.setAttribute("bookInfo", info);
        request.getRequestDispatcher("/WEB-INF/pages/book_information.jsp").forward(request,response);
    }
}
