/***
 *  Modify by 李家君 on 2018.06.13
 * 实现doPost方法
 */

package servlet;

import DaoImp.OrderDaoImp;
import DaoImp.ShoppingCartDaoImp;
import Util.OrderGenerator;
import bean.BookItem;
import bean.Customer;
import bean.Order;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCartServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        List<Order> orders =  (List<Order>) session.getAttribute("orders");
        List<BookItem> cartItems = (List<BookItem>) session.getAttribute("cartItems");
        Customer customer = (Customer)session.getAttribute("currentCustomer");

        //解决购物车没有商品但是点击立即支付时会产生一个空账单的bug --魏春光
        if(cartItems == null || cartItems.size() == 0){
            request.getRequestDispatcher("/shoppingCart").forward(request,response);
            return;
        }

        Order order = OrderGenerator.createOrder(cartItems);

        if(customer.getMoney() < order.getMoney()){
            // 提示用户金额不够....
            request.setAttribute("isEnough", "false");
            request.getRequestDispatcher("/shoppingCart").forward(request,response);
            return;
        }
        //先加进数据库
        OrderDaoImp.addOrder(order, ((Customer)session.getAttribute("currentCustomer")).getId());
        if(orders == null){
            orders = new ArrayList<>();
        }

        //再从数据库中取出来
        order = OrderDaoImp.getOrder(((Customer)session.getAttribute("currentCustomer")).getId());
        orders.add(order);

        //删除购物车
        for (BookItem item: cartItems) {
            ShoppingCartDaoImp.removeItem(item, ((Customer)session.getAttribute("currentCustomer")).getId());
        }

        // 更新Session
        session.setAttribute("orders", orders);
        session.setAttribute("cartItems", null);

        // 更新顾客余额
        customer.minMoney(order.getMoney());
        session.setAttribute("currentCustomer", customer);

        // 重新回到购物车页面
        request.getRequestDispatcher("/user").forward(request,response);
    }
}
