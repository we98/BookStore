package servlet;

import DaoImp.BookDaoImp;
import DaoImp.CommentDaoImp;
import Util.DateGenerator;
import bean.BookInfo;
import bean.Comment;
import bean.Customer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AddCommentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        resp.setContentType("text/html; charset=utf-8");
        req.setCharacterEncoding("utf-8");

        HttpSession session = req.getSession();

        Customer customer = (Customer)session.getAttribute("currentCustomer");

        String commentInfo = req.getParameter("commentInfo");
        int bookid = Integer.parseInt(req.getParameter("bookid"));

        Comment comment = new Comment();
        comment.setBookId(bookid);
        comment.setCustomerId(customer.getId());
        comment.setDate(DateGenerator.getCurrentTime());
        comment.setAuthor(customer.getName());
        comment.setCommentInfo(commentInfo);
        CommentDaoImp.addComment(comment);

        // 重新跳转到book_information.jsp
        BookInfo info = BookDaoImp.getBook(bookid);
        req.setAttribute("bookInfo", info);
        req.getRequestDispatcher("/WEB-INF/pages/book_information.jsp").forward(req, resp);
    }
}
