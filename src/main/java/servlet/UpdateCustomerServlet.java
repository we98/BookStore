package servlet;

import DaoImp.CustomerDaoImp;
import bean.Customer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class UpdateCustomerServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();

        String name = req.getParameter("name");
        String address = req.getParameter("address");
        String email = req.getParameter("email");

        Customer customer = (Customer)session.getAttribute("currentCustomer");

        name = name.trim();
        address = address.trim();
        email = email.trim();

        customer.setName(name);
        customer.setAddress(address);
        customer.setEmail(email);
        session.setAttribute("currentCustomer", customer);
        //更新到数据库
        CustomerDaoImp.update(customer);
        // 重新跳转到个人主页
        req.getRequestDispatcher("/WEB-INF/pages/customer_information.jsp").forward(req, resp);
    }
}
