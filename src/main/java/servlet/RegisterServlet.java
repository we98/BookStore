/***
 *  Modify by 李家君 on 2018.06.13
 * 实现doPost方法
 */

package servlet;

import DaoImp.CustomerDaoImp;
import DaoImp.OrderDaoImp;
import DaoImp.ShoppingCartDaoImp;
import bean.Customer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class RegisterServlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=utf-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        String cPhone = request.getParameter("phoneNumber");
        String cPassword = request.getParameter("password");
        String cEmail = request.getParameter("email");
        String cProvince = request.getParameter("province");
        String cCity = request.getParameter("city");
        String cDistrict = request.getParameter("district");

        Customer customer = CustomerDaoImp.find(cPhone);// 由于数据库实现原因，小概率会造成误查
        if(customer != null){
            // 该账户先前已创建则跳转到登陆页面
            request.setAttribute("error", "inline");
            //request.setAttribute("phoneNumber", cPhone);
            //request.setAttribute("password", cPassword);
            request.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(request,response);
        }
        else{
            // 新建用户
            customer = new Customer();
            customer.setPhoneNumber(cPhone);
            customer.setPassword(cPassword);
            customer.setEmail(cEmail);
            customer.setAddress(cProvince + cCity + cDistrict);;
            customer.setName("user-" + customer.getPhoneNumber());
            CustomerDaoImp.add(customer);
            customer = CustomerDaoImp.find(cPhone);  // 为了获取数据库生成的customerID

            session.setAttribute("currentCustomer", customer);
            session.setAttribute("cartItems", ShoppingCartDaoImp.getCustomerItem(customer));
            session.setAttribute("orders", OrderDaoImp.getOrders(customer.getId()));
            request.getRequestDispatcher("/WEB-INF/pages/index.jsp").forward(request,response);
        }
    }
}
