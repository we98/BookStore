/***
 *  @brief 检测是否登陆的过滤器
 *  @author 李家君
 *  @date 2018.06.11
 */


package filter;
import bean.Customer;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class LoginFilter implements Filter{

    private String loginURL;            // 登陆界面的URL

    @Override
    public void init(FilterConfig filterConfig) {
        loginURL = filterConfig.getInitParameter("loginURL");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;


        Customer c = (Customer) httpServletRequest.getSession().getAttribute("currentCustomer");
        if (c == null) {                          // 用户并没有登陆过
            httpServletResponse.sendRedirect(loginURL);
        }
        else{
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
