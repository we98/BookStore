package Util;

import java.text.DecimalFormat;

public class MoneyFormat {

    public static String printMoney(float money){
        DecimalFormat decimalFormat=new DecimalFormat(".00");
        return decimalFormat.format(money);
    }



}
