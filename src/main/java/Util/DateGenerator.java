/**
 * @brief 获取当前时间辅助类
 * @author 李家君
 * @date 2018.06.13
 */

package Util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateGenerator {

    public static String getCurrentTime(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(new Date());
    }
}
