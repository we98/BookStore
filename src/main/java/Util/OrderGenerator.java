package Util;

import DaoImp.OrderDaoImp;
import bean.BookItem;
import bean.Order;
import java.util.List;

public class OrderGenerator {

    public static Order createOrder(List<BookItem> items){
        Order order = new Order();
        order.setDate(DateGenerator.getCurrentTime());
        order.setOrderItems(items);
        return order;
    }
}
