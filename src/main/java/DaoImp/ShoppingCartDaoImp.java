package DaoImp;

import bean.BookItem;
import bean.Customer;
import dbController.MyDatabaseController;

import java.sql.*;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartDaoImp {

    static public boolean addItem(BookItem item, int customerId){

        // 往购物车中加入记录
        boolean flag = false;
        if (item == null) return false;
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            flag = (stmt.executeUpdate("INSERT INTO cartitem(customerid,price,bookid,quantity) VALUES(" +
                    customerId + "," +
                    item.getInfo().getPrice() * item.getQuantity() + "," +
                    item.getInfo().getBookId() + "," +
                    item.getQuantity() + ")")) > 0;
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return flag;
    }

    static public boolean removeItem(BookItem item, int customerId){

        // 删除购物车的一条记录
        //一个用户的购物车中不会有重复的book,所以用bookid和customerid来判断

        boolean flag = false;
        if (item == null) return false;
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            flag = (stmt.executeUpdate("DELETE FROM cartitem WHERE bookid=" + item.getInfo().getBookId() + " AND customerid=" + customerId)) > 0;
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    static public boolean modifyItem(BookItem item, int customerId) {
        //修改购物车
        boolean flag = false;
        if (item == null) return flag;
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            String s = "UPDATE cartitem SET quantity=" + item.getQuantity() + ", price=" + item.getMoney()
                    + " WHERE bookid=" + item.getBookId() + " AND customerid=" + customerId;
            flag = stmt.executeUpdate(s) > 0;
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    static public boolean findItem(int bookId, int customerId) {
        //寻找购物车项，如果有就用修改，没有就用添加
        boolean flag = false;
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS num FROM cartitem WHERE bookid=" + bookId + " AND customerid=" + customerId);
            if (rs.next()) {
                flag = rs.getInt("num") > 0;
            }
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    static public List<BookItem> getCustomerItem(Customer customer){

        // 查出该用户购物车所有数据
        List<BookItem> list = new ArrayList<>();
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM cartitem WHERE customerid=" + customer.getId());
            while (rs.next()) {
                BookItem item = new BookItem();
                item.setBookId(rs.getInt("bookid"));
                item.setInfo(BookDaoImp.getBook(item.getBookId()));
                item.setQuantity(rs.getInt("quantity"));
                list.add(item);
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

}
