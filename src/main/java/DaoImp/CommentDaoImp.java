
package DaoImp;

import bean.Comment;
import dbController.MyDatabaseController;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CommentDaoImp {
    public static boolean addComment(Comment comment) {
        boolean flag = false;
        if (comment == null) return false;
        //获取当天时间
        Date day=new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            flag = (stmt.executeUpdate("INSERT INTO comment(commentid,commentinfo,customerid,bookid,dateinfo) " + "VALUES (" +
                    comment.getCommentId() + "," +
                    "'" + comment.getCommentInfo() + "'," +
                    comment.getCustomerId() + "," +
                    comment.getBookId() + "," +
                    "'" + df.format(day) + "')")) > 0;
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return flag;
    }

    public static List<Comment> getCommentsByBook(int bookId) {
        List<Comment> comments = new ArrayList<>();
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM comment WHERE bookid = " + bookId);
            while (rs.next()) {
                Comment comment = new Comment();
                comment.setCommentId(rs.getInt("commentid"));
                comment.setCommentInfo(rs.getString("commentinfo"));
                comment.setCustomerId(rs.getInt("customerid"));
                comment.setDate(rs.getString("dateinfo"));
                comments.add(comment);
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return comments;
    }
}
