package DaoImp;

import bean.BookInfo;
import bean.BookItem;
import bean.Comment;
import bean.Order;
import dbController.MyDatabaseController;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderDaoImp {


    static public List<Order> getOrders(int customerId){
        // 根据CustomerId取出所有的Order和OrderItem
        Connection conn = MyDatabaseController.getMyConnection();
        List<Order> orders = new ArrayList<>();
        try {
            //查询所有的order
            Statement stmtOfOrder = conn.createStatement();
            ResultSet resultOfOrder = stmtOfOrder.executeQuery("SELECT * FROM orderinfo WHERE customerid=" + customerId);
            while (resultOfOrder.next()) {
                Order order = new Order();
                order.setDate(resultOfOrder.getString("dateinfo"));
                order.setOrderId(resultOfOrder.getInt("orderid"));
                List<BookItem> items = new ArrayList<>();
                //根据orderid查询对应的items
                Statement stmtOfItem = conn.createStatement();
                ResultSet resultOfItem = stmtOfItem.executeQuery("SELECT * FROM orderitem WHERE orderid=" + order.getOrderId());
                while (resultOfItem.next()) {
                    BookItem item = new BookItem();
                    //根据bookid查询对应的book
                    item.setBookId(resultOfItem.getInt("bookid"));
                    Statement stmtOfBook = conn.createStatement();
                    ResultSet resultOfBook = stmtOfBook.executeQuery("SELECT * FROM book WHERE bookid=" + item.getBookId());
                    BookInfo book = new BookInfo();
                    if (resultOfBook.next()) {
                        book.setName(resultOfBook.getString("name"));
                        book.setSrc(resultOfBook.getString("src"));
                        book.setPrice(resultOfBook.getFloat("price"));
                        book.setBookId(resultOfBook.getInt("bookid"));
                    }
                    item.setInfo(book);
                    item.setQuantity(resultOfItem.getInt("quantity"));

                    resultOfBook.close();
                    stmtOfBook.close();
                    items.add(item);
                }
                resultOfItem.close();
                stmtOfItem.close();

                order.setOrderItems(items);
                orders.add(order);
            }
            resultOfOrder.close();
            stmtOfOrder.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    static public boolean addOrder(Order order, int customerId){

        // 根据CustomerId向Order表和OrderItem表中插入数据
        if (order == null) return false;
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            //获取当天时间
            Date day=new Date();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Statement stmt = conn.createStatement();
            //插入order
            stmt.executeUpdate("INSERT INTO orderinfo(dateinfo,customerid,price) VALUES(" +
                    "'" + df.format(day) +"'," +
                     + customerId + "," +
                    order.getMoney() + ")");
            //插入orderitem
            int id = 0;
            ResultSet rs = stmt.executeQuery("SELECT MAX(orderid) AS id FROM orderinfo");
            if (rs.next()) {
                id = rs.getInt("id");//获取orderId
            }
            if (id == 0)
                return false;
            List<BookItem> books = order.getOrderItems();
            for (BookItem book: books) {
                stmt.executeUpdate("INSERT INTO orderitem(orderid, price, date_id, bookid, quantity) VALUES(" +
                        id +"," + book.getMoney() + ",'" +
                        df.format(day) + "'," + book.getBookId() + "," + book.getQuantity() +")");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    static public Order getOrder(int customerId){
        // 根据CustomerId取出最新的Order
        Connection conn = MyDatabaseController.getMyConnection();
        Order order = new Order();
        try {
            //查询订单号最大的Order
            Statement stmtOfOrder = conn.createStatement();
            ResultSet resultOfOrder = stmtOfOrder.executeQuery("SELECT * FROM orderinfo WHERE customerid=" + customerId + " AND orderid=(SELECT MAX(orderid) from orderinfo)");
            if (resultOfOrder.next()) {
                order.setDate(resultOfOrder.getString("dateinfo"));
                order.setOrderId(resultOfOrder.getInt("orderid"));
                List<BookItem> items = new ArrayList<>();
                //根据orderid查询对应的items
                Statement stmtOfItem = conn.createStatement();
                ResultSet resultOfItem = stmtOfItem.executeQuery("SELECT * FROM orderitem WHERE orderid=" + order.getOrderId());
                while (resultOfItem.next()) {
                    BookItem item = new BookItem();
                    //根据bookid查询对应的book
                    item.setBookId(resultOfItem.getInt("bookid"));
                    Statement stmtOfBook = conn.createStatement();
                    ResultSet resultOfBook = stmtOfBook.executeQuery("SELECT * FROM book WHERE bookid=" + item.getBookId());
                    BookInfo book = new BookInfo();
                    if (resultOfBook.next()) {
                        book.setName(resultOfBook.getString("name"));
                        book.setSrc(resultOfBook.getString("src"));
                        book.setPrice(resultOfBook.getFloat("price"));
                    }
                    item.setInfo(book);
                    item.setQuantity(resultOfItem.getInt("quantity"));

                    resultOfBook.close();
                    stmtOfBook.close();
                    items.add(item);
                }
                resultOfItem.close();
                stmtOfItem.close();

                order.setOrderItems(items);
            }
            resultOfOrder.close();
            stmtOfOrder.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return order;
    }

    static public boolean removeOrderId(int orderId) {
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            stmt.execute("DELETE FROM orderitem WHERE orderid=" + orderId);
            stmt.execute("DELETE FROM orderinfo WHERE orderid=" + orderId);
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
}
