package DaoImp;

import bean.BookInfo;
import dbController.MyDatabaseController;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BookDaoImp {
    public static List<BookInfo> getBooksByType(int type) {
        //根据书籍的类型查找数据库
        List<BookInfo> books = new ArrayList<>();
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM book WHERE type=" + type);

                while(rs.next()) {
                    BookInfo book = new BookInfo();
                    book.setDescription(rs.getString("description"));
                    book.setName(rs.getString("name"));
                    book.setType(rs.getInt("type"));
                    book.setPublisher(rs.getString("publisher"));
                    book.setBookId(rs.getInt("bookid"));
                    book.setSrc(rs.getString("src"));
                    book.setPrice(rs.getFloat("price"));
                    books.add(book);
            }
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    public static BookInfo getBook(int id) {
        //根据书籍的id查找书籍信息
        BookInfo book = null;
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM book WHERE bookid=" + id);
            if (rs.next()) {//最多一行
                book = new BookInfo();
                book.setDescription(rs.getString("description"));
                book.setName(rs.getString("name"));
                book.setType(rs.getInt("type"));
                book.setPublisher(rs.getString("publisher"));
                book.setBookId(rs.getInt("bookid"));
                book.setSrc(rs.getString("src"));
                book.setPrice(rs.getFloat("price"));
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return book;

    }
}
