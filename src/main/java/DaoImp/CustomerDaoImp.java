package DaoImp;

import bean.*;
import dbController.MyDatabaseController;

import java.sql.*;

public class CustomerDaoImp {
    //用于注册逻辑
    static public Customer find(String phonenumber){
        // 从数据库中找到所求的用户
        Customer customer = null;
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            /*ResultSet rs = stmt.executeQuery("SELECT * FROM customer WHERE phonenumber='" +
                    phonenumber + "' AND password='" + pswd + "'");*/
            ResultSet rs = stmt.executeQuery("SELECT * FROM customer WHERE phonenumber='" +
                    phonenumber + "'");
            if(rs.next()) {//最多一行
                customer = new Customer();
                customer.setId(rs.getInt("customerid"));
                customer.setAddress(rs.getString("address"));
                customer.setEmail(rs.getString("email"));
                customer.setMoney(rs.getFloat("money"));
                customer.setPassword(rs.getString("password"));
                customer.setName(rs.getString("name"));
                customer.setPhoneNumber(rs.getString("phonenumber"));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    //用于登录逻辑
    static public Customer find(String phonenumber, String pswd){
        // 从数据库中找到所求的用户
        Customer customer = null;
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM customer WHERE phonenumber='" +
                    phonenumber + "' AND password='" + pswd + "'");
            if(rs.next()) {//最多一行
                customer = new Customer();
                customer.setId(rs.getInt("customerid"));
                customer.setAddress(rs.getString("address"));
                customer.setEmail(rs.getString("email"));
                customer.setMoney(rs.getFloat("money"));
                customer.setPassword(rs.getString("password"));
                customer.setName(rs.getString("name"));
                customer.setPhoneNumber(rs.getString("phonenumber"));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    static public boolean add(Customer customer){
        boolean flag = false;
        if (customer == null) return false;
        //if(find(customer.getPhoneNumber(), customer.getPassword()) != null){
        if(find(customer.getPhoneNumber()) != null){
            System.out.println("该用户已经存在！！！");   // 调试用
        }
        else{
            // 把新用户写入数据库
            Connection conn = MyDatabaseController.getMyConnection();
            try {
                Statement stmt = conn.createStatement();
                flag = stmt.execute("INSERT INTO customer(customerid, money, name, password, address, phonenumber, email)" +
                        " VALUES('" + customer.getId() + "',"
                                    + customer.getMoney() + ",'"
                                    + customer.getName() + "','"
                                    + customer.getPassword() + "','"
                                    + customer.getAddress() + "','"
                                    + customer.getPhoneNumber() + "','"
                                    + customer.getEmail() + "')");
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return flag;
    }

    static public boolean update(Customer customer){
        // 更新用户信息到数据库
        boolean flag = false;
        if (customer == null) return false;
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            flag = (stmt.executeUpdate("UPDATE customer " +
                    "SET money=" + customer.getMoney() +
                    ",name='" + customer.getName() +
                    "',password='" + customer.getPassword() +
                    "',address='" + customer.getAddress() +
                    "',phonenumber='" + customer.getPhoneNumber() +
                    "',email='" + customer.getEmail() + "' WHERE customerid='" + customer.getId() + "'")) > 0;
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    static public String getName(int id) {
        String name = "";
        Connection conn = MyDatabaseController.getMyConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT name from customer WHERE customerid=" + id);
            if (rs.next()) {
                name = rs.getString("name");
            }
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return name;
    }
}
