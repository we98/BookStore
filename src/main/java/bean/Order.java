/**
 * @brief 订单类（对应数据库中Order表）
 * @author 李家君
 * @date 2018.06.11
 */

package bean;

import java.util.List;


public class Order {

    private String date;
    private List<BookItem> orderItems;
    private float money;
    private int orderId;
    // 不需要存储customerId, 因为在读取时已经知道这个Order属于哪一个Customer

    public Order(){

    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate(){
        return date;
    }

    public void setOrderItems(List<BookItem> orderItems) {
        this.orderItems = orderItems;

        if(orderItems != null)
            for(int i = 0; i < orderItems.size(); i++)
                this.money += orderItems.get(i).getMoney();
    }

    public List<BookItem> getOrderItems() {
        return orderItems;
    }

    public float getMoney(){
        return money;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
    public int getOrderId(){
        return orderId;
    }
}
