/***
 *  Modify by 李家君 on 2018.06.11
 * 为类加入HttpSessionBindingListner接口
 */


package bean;

import DaoImp.CustomerDaoImp;
import DaoImp.OrderDaoImp;
import DaoImp.ShoppingCartDaoImp;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import java.awt.print.Book;
import java.util.List;


public class Customer implements HttpSessionBindingListener{

    private int id;
    private String phoneNumber;
    private String email;
    private String password;
    private String address;
    private String name;
    private float money;

    public Customer(){

    }

    public int getId(){
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber(){
        return phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail(){
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword(){
        return password;
    }


    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress(){
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setMoney(float money){
        this.money = money;
    }

    public void addMoney(float money){
        this.money += money;
    }

    public void minMoney(float money){ this.money -= money; }

    public float getMoney(){
        return money;
    }

    @Override
    public void valueBound(HttpSessionBindingEvent httpSessionBindingEvent) {
        // 这里可以加上统计当前在线用户数的代码
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent httpSessionBindingEvent) {

        // 更新购物车
        List<BookItem> cartItems = (List<BookItem>)httpSessionBindingEvent.getSession().getAttribute("cartItems");
        if(cartItems != null){
            for(int i = 0; i < cartItems.size(); i++){
                if (ShoppingCartDaoImp.findItem(cartItems.get(i).getBookId(), this.getId()))
                    ShoppingCartDaoImp.modifyItem(cartItems.get(i), this.getId());
                else
                    ShoppingCartDaoImp.addItem(cartItems.get(i), this.getId());
            }
        }

        // 更新客户信息
        CustomerDaoImp.update(this);
    }
}
