/**
 * @brief 书本信息类（对应数据库的Order表）
 * @author 李家君
 * @date 2018.06.11
 */

/**
 * @brief 加入bookTypeMap
 * @author 李家君
 * @date 2018.06.14
 */


package bean;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

public class BookInfo{

    private static Map<Integer,String> bookTypeMap;
    {
        bookTypeMap = new HashMap<Integer, String>();
        bookTypeMap.put(1, "IT精选");
        bookTypeMap.put(2, "文海畅游");
        bookTypeMap.put(3, "小说推理");
        bookTypeMap.put(4, "数学逻辑");
    };

    private int bookId;
    private String name;
    private int type;
    private String description;
    private String publisher;
    private float price;
    private String src;

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getType() {
        return bookTypeMap.get(type);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPrice() {
        return price;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getSrc() {
        return src;
    }
}



