/**
 * @brief 书本项类（对应数据库的CartItem表、OrderItem表）
 * @author 李家君
 * @date 2018.06.11
 */


package bean;

public class BookItem {

    private BookInfo info;
    private int quantity;
    private float money;
    public int bookId;

    public BookItem(){

    }

    public void setInfo(BookInfo info) {
        this.info = info;
    }

    public BookInfo getInfo() {
        return info;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
        this.money = quantity * info.getPrice();
    }

    public int getQuantity() {
        return quantity;
    }

    public void addQuantity(int i){
        this.quantity += i;
        this.money += (i*info.getPrice());
    }

    public float getMoney() {
        return money;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getBookId() {
        return bookId;
    }
}
