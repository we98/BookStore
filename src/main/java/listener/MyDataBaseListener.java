package listener;

import dbController.MyDatabaseController;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyDataBaseListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent arg0) {

        ServletContext context = arg0.getServletContext();
        String driver = context.getInitParameter("DBDriver");
        String url = context.getInitParameter("DBUrl");
        String user = context.getInitParameter("DBUser");
        String password = context.getInitParameter("DBPassword");

        MyDatabaseController.startConnection(driver, url, user, password);
    }

    public void contextDestroyed(ServletContextEvent arg0) {
        MyDatabaseController.endConnection();
    }

}