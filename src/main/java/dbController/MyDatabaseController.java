package dbController;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyDatabaseController {

    private static Connection connection;

    public static Connection getMyConnection(){
        return connection;
    }

    public static void startConnection(String driver, String url, String user, String password){
        try{
            Class.forName(driver);
            connection = DriverManager.getConnection(url,user,password);
            System.out.println("successfully connect");
        }
        catch(ClassNotFoundException e){
            e.printStackTrace();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    public static void endConnection(){
        if(connection != null){
            try{
                connection.close();
                System.out.println("successfully end");
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
    }
}
